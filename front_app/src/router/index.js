import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/Homepage'
import ContactDetail from '@/components/ContactDetail'
import NewContactDetail from '@/components/NewContactDetail'
import PageNotFound from '@/components/PageNotFound'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Homepage',
      component: Homepage
    },
    {
      path: '/create/',
      name: 'NewContactDetail',
      component: NewContactDetail
    },
    {
      path: '/contact/:_id',
      name: 'ContactDetail',
      component: ContactDetail
    },
    {
      path: '*',
      name: 'PageNotFound',
      component: PageNotFound
    }
  ]
})
