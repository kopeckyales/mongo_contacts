import {serverRequest} from './networkService'

async function getContactList () {
  return serverRequest('contacts')
}

async function getContact (id) {
  return serverRequest(`contacts/${id}`)
}

async function editContact (contact) {
  return serverRequest(`contacts/${contact['_id']}`, 'PUT', contact)
}

async function deleteContact (id) {
  return serverRequest(`contacts/${id}`, 'DELETE')
}

async function createContact (data) {
  return serverRequest('contacts', 'POST', data)
}

export {
  getContactList,
  getContact,
  editContact,
  deleteContact,
  createContact
}
