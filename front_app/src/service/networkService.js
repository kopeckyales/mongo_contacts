
const SERVER_URL = 'http://localhost:3000'

async function serverRequest (url, method = 'GET', data = null) {
  url = `${SERVER_URL}/${url}`
  let options = {
    method: method,
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  }
  if (data != null) {
    options['body'] = JSON.stringify(data)
  }
  const response = await fetch(url, options)
  if (response.status !== 200) {
    throw new Error(response.statusText)
  }
  return response.json()
}

export {
  serverRequest
}
