// tslint:disable:ordered-imports
import 'reflect-metadata';
import * as bodyParser from "body-parser";
import * as express from "express";
import {Container} from "inversify";
import {InversifyExpressServer} from "inversify-express-utils";

import TYPES from "./api/constant/types";
import CZPhoneNumberValidator from "./api/services/CZPhoneNumberValidator";
import {IPhoneNumberValidator} from "./api/services/IPhoneNumberValidator";

import "./api/controllers/DefaultController";
import "./api/controllers/ContactsController";
import ContactsService from "./api/services/ContactsService";
import * as mongoose from "mongoose";
import * as cors from "cors";

const PORT = 3000;
const CONNECTION_STRING = 'mongodb://localhost:27017/contacts';

const container = new Container();
container.bind<IPhoneNumberValidator>(TYPES.PhoneNumberValidator).to(CZPhoneNumberValidator);
container.bind<ContactsService>(TYPES.ContactsService).to(ContactsService);

const server = new InversifyExpressServer(container);
server.setConfig((app: express.Application) => {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cors());
    (mongoose as any).Promise = global.Promise;
    mongoose.connect(CONNECTION_STRING, {useNewUrlParser: true});
});

const appInstance = server.build();

appInstance.listen(PORT, () => {
    // tslint:disable-next-line:no-console
    console.log('Express server listening on port ' + PORT);
});