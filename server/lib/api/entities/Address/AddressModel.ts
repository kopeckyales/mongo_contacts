import * as mongoose from 'mongoose';
import {IAddress} from './IAddress';

export const AddressSchema = new mongoose.Schema({
    city: {
        type: String,
        required: true,
    },
    streetAndHouseNumber: {
        type: String,
        required: true,
    },
    zipCode: {
        type: String,
        required: true,
    },
    note: {
        type: String,
    },
});

interface IAddressModel extends IAddress, mongoose.Document { }
export const Address = mongoose.model<IAddressModel>('Address', AddressSchema);