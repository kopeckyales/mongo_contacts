export interface IAddress {
    city: string;
    streetAndHouseNumber: string;
    zipCode: string;
    note: string;
}