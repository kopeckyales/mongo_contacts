import * as mongoose from 'mongoose';
import {AddressSchema} from "../Address/AddressModel";
import {IContact} from "./IContact";

const Schema = mongoose.Schema;

export const ContactSchema = new Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    emails: {
        type: [String],
        validate: {
            validator: (value) => {
                return value.length > 0;
            },
        },
    },
    phoneNumbers: {
        type: [String],
    },
    addresses: {
        type: [AddressSchema],
    },
});

interface IContactModel extends IContact, mongoose.Document { }
export const Contact = mongoose.model<IContactModel>('Contact', ContactSchema);