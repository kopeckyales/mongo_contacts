import {Address} from "../Address/AddressModel";

export interface IContact {
    firstName: string;
    lastName: string;
    emails: string[];
    phoneNumbers: string[];
    addresses: Address[];
}