const TYPES = {
    PhoneNumberValidator: Symbol.for('PhoneNumberValidator'),
    ContactsService: Symbol.for('ContactsService'),
};

export default TYPES;