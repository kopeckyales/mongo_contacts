import * as EmailValidator from 'email-validator';
import {inject, injectable} from "inversify";
import * as mongoose from "mongoose";
import {CastError} from "mongoose";
import TYPES from "../constant/types";
import {Contact} from "../entities/Contact/ContactModel";
import {IContact} from "../entities/Contact/IContact";
import {IPhoneNumberValidator} from "./IPhoneNumberValidator";

@injectable()
export default class ContactsService {

    constructor(@inject(TYPES.PhoneNumberValidator) private phoneNumberValidator: IPhoneNumberValidator) {
    }

    public async getAllContacts(): Promise<IContact[]> {
        return await Contact.find({});
    }

    public async getContact(id: string): Promise<IContact> {
        try {
            return await Contact.findById(id);
        } catch (e) {
            if (e instanceof CastError) {
                return null;
            }
            throw e;
        }
    }

    public async createContact(data: object) {
        const contact = new Contact(data);
        this.validateContact(contact);
        const item = await contact.save();
        return item._id;
    }

    public async editContact(id: string, data: object) {
        const contact = await this.getContact(id);
        (contact as any as mongoose.Document).set(data);
        this.validateContact(contact);
        (await (contact as any as mongoose.Document).save());
    }

    public async deleteContact(id: string) {
        await Contact.findOneAndRemove({_id: id}).exec();
    }

    private validateContact(contact: IContact) {
        contact.emails.map((email: string) => {
            if (!EmailValidator.validate(email)) {
                throw new Error(`Invalid email address: ${email}`);
            }
        });
        contact.phoneNumbers = contact.phoneNumbers.map((phoneNumber: string) => {
            return this.phoneNumberValidator.validate(phoneNumber);
        });
    }
}