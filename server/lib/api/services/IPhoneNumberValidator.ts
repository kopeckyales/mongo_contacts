export interface IPhoneNumberValidator {
    validate(phoneNumber: string): string;
}