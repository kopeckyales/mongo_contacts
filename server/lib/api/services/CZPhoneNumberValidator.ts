import {injectable} from "inversify";
import PhoneNumberValidationError from "./errors/PhoneNumberValidationError";
import {IPhoneNumberValidator} from "./IPhoneNumberValidator";

@injectable()
export default class CZPhoneNumberValidator implements IPhoneNumberValidator {
    public validate(phoneNumber: string): string {
        phoneNumber = phoneNumber.replace(/ |-|/gi, '');

        const simplifiedFormat = new RegExp(/^\d{9}$/);
        if (simplifiedFormat.test(phoneNumber)) {
            return `+420${phoneNumber}`;
        }

        const fullFormat = new RegExp(/^(\+|00)\d{1,4}\d{9}$/);
        if (fullFormat.test(phoneNumber)) {
            if (phoneNumber.startsWith('00')) {
                phoneNumber = phoneNumber.replace('00', '+');
            }
            return phoneNumber;
        }
        throw new PhoneNumberValidationError(`Invalid phone number: ${phoneNumber}`);
    }
}