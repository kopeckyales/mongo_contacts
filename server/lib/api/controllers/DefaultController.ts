import {controller, httpGet} from "inversify-express-utils";

@controller('/')
export class DefaultController {
    @httpGet('/')
    public get() {
        return {
            actions: [
                '[GET]/contacts',
                '[POST]/contacts',
                '[GET]/contacts/<id>',
                '[PUT]/contacts/<id>',
                '[DELETE]/contacts/<id>',
            ],
        };
    }
}