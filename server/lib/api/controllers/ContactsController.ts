import * as express from "express";
import {inject} from "inversify";
import {
    controller, httpDelete, httpGet, httpPost, httpPut, request, requestParam,
    response,
} from "inversify-express-utils";
import TYPES from "../constant/types";
import ContactsService from "../services/ContactsService";
import ContactNotFoundError from "../services/errors/ContactNotFoundError";

@controller('/contacts')
export class ContactsController {

    constructor(@inject(TYPES.ContactsService) private contactsService: ContactsService) {
    }

    @httpGet('/')
    public async getList() {
        return await this.contactsService.getAllContacts();
    }

    @httpPost('/')
    public async createItem(@request() req: express.Request) {
        try {
            return await this.contactsService.createContact(req.body);
        } catch (e) {
            return {status: 'NOK', error: e.message};
        }
    }

    @httpGet('/:contactId')
    public async getItem(@requestParam("contactId") id: string,
                         @request() req: express.Request,
                         @response() res: express.Response) {
        const item = await this.contactsService.getContact(id);
        if (item !== null) {
            return item;
        }
        res.sendStatus(404);
    }

    @httpPut('/:contactId')
    public async editItem(@requestParam("contactId") id: string,
                          @request() req: express.Request,
                          @response() res: express.Response) {
        try {
            await this.contactsService.editContact(id, req.body);
            return {status: 'OK'};
        } catch (e) {
            if (e instanceof ContactNotFoundError) {
                res.sendStatus(404);
            }
            return {status: 'NOK', error: e.message};
        }
    }

    @httpDelete('/:contactId')
    public async deleteItem(@requestParam("contactId") id: string,
                            @request() req: express.Request,
                            @response() res: express.Response) {
        try {
            await this.contactsService.deleteContact(id);
            return {status: 'OK'};
        } catch (e) {
            if (e instanceof ContactNotFoundError) {
                res.sendStatus(404);
            }
            return {status: 'NOK', error: e.message};
        }
    }
}