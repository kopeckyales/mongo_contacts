import {Address} from '../../../lib/api/entities/Address/AddressModel'

describe('Test address entity validations', () => {
    let validAddress;

    beforeEach(() => {
        validAddress = new Address();
        validAddress.city = 'Brno';
        validAddress.streetAndHouseNumber = 'Jihlavská 12';
        validAddress.zipCode = '62599';
        validAddress.note = 'Holiday house';
    });

    test('It should validate valid entity', () => {
        expect(validAddress.validateSync()).toEqual(undefined);
    });

    test('It should not require note', () => {
        validAddress.note = null;
        expect(validAddress.validateSync()).toEqual(undefined);
    });

    test('It should require city', () => {
        validAddress.city = null;
        let error = validAddress.validateSync();
        expect(error.errors).toHaveProperty('city');
    });

    test('It should require street', () => {
        validAddress.streetAndHouseNumber = null;
        let error = validAddress.validateSync();
        expect(error.errors).toHaveProperty('streetAndHouseNumber');
    });

    test('It should require zip code', () => {
        validAddress.zipCode = null;
        let error = validAddress.validateSync();
        expect(error.errors).toHaveProperty('zipCode');
    });
});