import {Contact} from '../../../lib/api/entities/Contact/ContactModel'

describe('Test contact entity validations', () => {
    let validContact;

    beforeEach(() => {
        validContact = new Contact();
        validContact.firstName = 'Jiří';
        validContact.lastName = 'Nováková';
        validContact.emails.push('jednicka.jednicka@email.cz');
    });

    test('It should validate valid entity', () => {
        expect(validContact.validateSync()).toEqual(undefined);
    });

    test('It should require first name', () => {
        validContact.firstName = null;
        let error = validContact.validateSync();
        expect(error.errors).toHaveProperty('firstName');
    });

    test('It should require last name', () => {
        validContact.lastName = null;
        let error = validContact.validateSync();
        expect(error.errors).toHaveProperty('lastName');
    });

    test('It should require at least one email', () => {
        validContact.emails = [];
        console.log(validContact.emails);
        let error = validContact.validateSync();
        expect(error.errors).toHaveProperty('emails');
    });
});