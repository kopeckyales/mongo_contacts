import "reflect-metadata"
import CZPhoneNumberValidator from '../../../lib/api/services/CZPhoneNumberValidator'
import PhoneNumberValidationError from '../../../lib/api/services/errors/PhoneNumberValidationError'

describe('Test czech phone number validating', () => {
    let validator = new CZPhoneNumberValidator();

    test('It should validate valid phone number', () => {
        let validPhoneNumber = '+420123456789';
        expect(validator.validate(validPhoneNumber)).toEqual(validPhoneNumber);
    });

    test('It should remove all white chars from valid phone number', () => {
        let validPhoneNumber = '+420 123 456 789';
        let expectedPhoneNumber = '+420123456789';
        expect(validator.validate(validPhoneNumber)).toEqual(expectedPhoneNumber);
    });

    test('It should convert 00 prefix to + prefix', () => {
        let validPhoneNumber = '00420 123 456 789';
        let expectedPhoneNumber = '+420123456789';
        expect(validator.validate(validPhoneNumber)).toEqual(expectedPhoneNumber);
    });

    test('It should remove all dashes from valid phone number', () => {
        let validPhoneNumber = '123-456-789';
        let expectedPhoneNumber = '+420123456789';
        expect(validator.validate(validPhoneNumber)).toEqual(expectedPhoneNumber);
    });

    test('It should add country prefix to simplified phone number', () => {
        let validPhoneNumber = '123 456 789';
        let expectedPhoneNumber = '+420123456789';
        expect(validator.validate(validPhoneNumber)).toEqual(expectedPhoneNumber);
    });

    test.each(['12345678', '42123456789', '(123) 456-789'])('It should throw an error on invalid number', (val) => {
        expect(() => validator.validate(val)).toThrow(PhoneNumberValidationError);
    });
});