export const MOCKED_CONTACTS = [
    {
        firstName: "Jiří",
        lastName: "Nováková",
        emails: ["jednicka.jednicka@email.cz", "jednicka1.jednicka1@email.cz"],
        phoneNumbers: ["+420123456789", "+420987654321"],
        addresses: [
            {
                city: "1",
                streetAndHouseNumber: "1",
                zipCode: "1",
                note: "1"
            },
            {
                city: "2",
                streetAndHouseNumber: "2",
                zipCode: "2",
                note: "2"
            }]
    },
    {
        firstName: "Jan",
        lastName: "Novák",
        emails: ["dvojka.dvojka@email.cz", "dvojka.dvojka@email.cz"],
        phoneNumbers: ["+420135792468", "+420975318642"],
        addresses: [
            {
                city: "1",
                streetAndHouseNumber: "1",
                zipCode: "1",
                note: "1"
            },
            {
                city: "2",
                streetAndHouseNumber: "2",
                zipCode: "2",
                note: "2"
            }]
    },
];