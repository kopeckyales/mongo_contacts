import ContactsService from "../../../../lib/api/services/ContactsService";
import {IContact} from "../../../../lib/api/entities/Contact/IContact";

export default class ContactsServiceMock extends ContactsService {
    public async getAllContacts(): Promise<IContact[]> {
        return new Promise((resolve, reject) => {
            resolve([]);
        });
    }

    public async getContact(id: string): Promise<IContact> {
        return (id === '1') ? <any>{} : null;
    }

    public async createContact(data: object) {
        return {id: '1'};
    }

    public async editContact(id: string, data: object) {
        return null;
    }

    public async deleteContact(id: string) {
        return null;
    }
}