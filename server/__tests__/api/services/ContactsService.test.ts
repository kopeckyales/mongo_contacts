import "reflect-metadata"
import ContactsService from "../../../lib/api/services/ContactsService";
import * as mongoose from "mongoose";
import {MOCKED_CONTACTS} from "./Mock/ContactModels.Mock";
import {Contact} from "../../../lib/api/entities/Contact/ContactModel";
import PhoneNumberValidatorMock from "./Mock/PhoneNumberValidator.Mock";

const CONNECTION_STRING = 'mongodb://localhost:27017/contactsTestDB';

describe('Test the contacts service', () => {
    (mongoose as any).Promise = global.Promise;
    let service = new ContactsService(new PhoneNumberValidatorMock());

    beforeAll(async () => {
        await mongoose.connect(CONNECTION_STRING, {useNewUrlParser: true});
    });

    afterAll(async () => {
        await mongoose.disconnect();
    });

    beforeEach(async () => {
        await Contact.deleteMany({}).exec();
    });

    test('It should create the item', async () => {
        let id1 = await service.createContact(MOCKED_CONTACTS[0]);
        let id2 = await service.createContact(MOCKED_CONTACTS[1]);
        expect(id1).not.toEqual(id2);
    });

    test('It should return all items', async () => {
        let id1 = await service.createContact(MOCKED_CONTACTS[0]);
        let id2 = await service.createContact(MOCKED_CONTACTS[1]);
        let list = await service.getAllContacts();
        expect(list.length).toEqual(2);
    });

    test('It should persist the item', async () => {
        let id = await service.createContact(MOCKED_CONTACTS[0]);
        let item = await service.getContact(id);
        // JSON things needs to be done or "Compared values have no visual difference error" occurs.
        expect(JSON.parse(JSON.stringify(item))).toMatchObject(MOCKED_CONTACTS[0]);
    });

    test('It should edit the item', async () => {
        let id = await service.createContact(MOCKED_CONTACTS[0]);
        await service.editContact(id,MOCKED_CONTACTS[1]);
        let updated = await service.getContact(id);
        // JSON things needs to be done or "Compared values have no visual difference error" occurs.
        expect(JSON.parse(JSON.stringify(updated))).toMatchObject(MOCKED_CONTACTS[1]);
    });

    test('It should remove the item', async () => {
        let id = await service.createContact(MOCKED_CONTACTS[0]);
        await service.deleteContact(id);
        let item = await service.getContact(id);
        expect(item).toEqual(null);
    });
});