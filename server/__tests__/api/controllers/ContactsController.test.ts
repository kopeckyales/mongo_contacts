import "reflect-metadata"
import {ContactsController} from "../../../lib/api/controllers/ContactsController";
import ContactsServiceMock from "../services/Mock/ContactsService.Mock";
import PhoneNumberValidatorMock from "../services/Mock/PhoneNumberValidator.Mock";

describe('Test the contacts controller',() => {
    let controller = new ContactsController(new ContactsServiceMock(new PhoneNumberValidatorMock()));
    test('It should return all stored contacts',async () => {
        expect(await controller.getList()).toEqual([]);
    });
    test('It should return contact',async () => {
        expect(await controller.getItem('1', <any>{}, <any>{})).toEqual({});
    });
    test('It should return new id',async () => {
        expect(await controller.createItem(<any>{})).toEqual({id: '1'});
    });
    test('It should return 404 response',async () => {
        let res = <any>{
            status: 200,
            sendStatus(stat) {
                this.status = stat
            }
        };
        await controller.getItem('2', <any>{}, res);
        expect(res.status).toEqual(404);
    });
    test('It should return json OK',async () => {
        expect(await controller.deleteItem('1', <any>{}, <any>{})).toEqual({status: 'OK'});
        expect(await controller.editItem('1', <any>{}, <any>{})).toEqual({status: 'OK'});
    });
});