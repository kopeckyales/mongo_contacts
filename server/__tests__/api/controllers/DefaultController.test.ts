import "reflect-metadata"
import {DefaultController} from "../../../lib/api/controllers/DefaultController";

describe('Test the / GET request',() => {
    test('It should return json with all possible actions', () => {
        let controller = new DefaultController();
        expect(controller.get()['actions']).toEqual(
            [
                '[GET]/contacts',
                '[POST]/contacts',
                '[GET]/contacts/<id>',
                '[PUT]/contacts/<id>',
                '[DELETE]/contacts/<id>'
            ]);
    });
});